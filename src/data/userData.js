// import axios from "axios"
import API from "../utils/API"

export const GET_ALL_USER =async ()=>{
    const response = await API.get("/users")
    
    return response.data;
}

export const CREATE_USER =async (NewUsers)=>{
    const response = await API.post("/users",NewUsers)
    return response.data;
}

export const GET_ONE = async(id)=>{
    const response = await API(`/users/${id}`)
    return response.data;
}
