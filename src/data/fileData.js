import API from "../utils/API";

export const file_Upload = async (data)=>{
    const response = await API.post("/files/upload",data,{})
    return response.data;
}