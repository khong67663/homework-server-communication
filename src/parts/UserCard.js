import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import { Link } from 'react-router-dom';

function UserCard(props) {
  return (
    <Card style={{ width: '18rem',marginBottom:'20px',height:'25rem' }}>
      <Card.Img variant="top" style={{ height:'220px' }} src={props.user.avatar} 
          onError={({ currentTarget }) => {
            currentTarget.onerror = null; // prevents looping
            currentTarget.src="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png";
            }}/>
      <Card.Body>
        <Card.Title>{props.user.name}</Card.Title>
        <Card.Text>{props.user.role}</Card.Text>
        <Card.Text>{props.user.email}</Card.Text>
        <Link to={`${props.user.id}`}>
          <Button variant="primary">View</Button>
        </Link>
        
      </Card.Body>
    </Card>
  );
}

export default UserCard;