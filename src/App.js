
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as BigRouter, Routes, Route } from 'react-router-dom';
import UserNavBar from './parts/UserNavBar';
// import UserCard from './parts/UserCard';
import Homepage from './pages/Homepage';
import AllofUser from './pages/AllofUser';
import CreateUser from './pages/CreateUser';
import ViewUserProfile from './pages/ViewUserProfile';

function App() {
  return (
    <div>
      <BigRouter>
        <UserNavBar/>
        <Routes>
          <Route path="" index element={<Homepage/>}/>
          <Route path="allUsers" element={<AllofUser/>}/>
          <Route path="allUsers/:id" element={<ViewUserProfile/>}/>
          <Route path="createusers" element={<CreateUser/>}/>
        </Routes>
      </BigRouter>
    </div>
  );
}

export default App;
