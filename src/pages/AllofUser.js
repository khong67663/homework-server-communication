import React from 'react'
import { useState,useEffect } from 'react'
import {GET_ALL_USER} from "../data/userData"
import UserCard from '../parts/UserCard';
import {TailSpin} from "react-loader-spinner";

const AllofUser = (props) => {

    const[users,setUser]=useState([]);
    const[isLoading,setIsLoading]=useState(true);
    const[keyword,setKeyword]=useState("");
    const[filteredUser,setFilterUser]=useState([]);  

    
    useEffect(()=>{
        GET_ALL_USER()
        .then((response)=>
        { setUser(response); 
          setIsLoading(false);       
          setFilterUser(response);
        })
        .catch((error)=>("Error:",error))
    },[])

    console.log("all user ",users)

    console.log(
      "All User:",
      users.sort((usera,userb)=>
        usera.id > userb.id ? -1 : usera.id < userb.id ? 1 :0
      )
    )

    let filteredList = null;

  useEffect(() => {
    if (keyword === null || keyword.trim() === "") {
      filteredList = users;
    } else {
      filteredList = users.filter((user) => user.name.toLowerCase().startsWith(keyword.toLowerCase()));
    }

    filteredList=users.filter((user)=>user.name.toLowerCase().startsWith(keyword.toLowerCase()));
    setFilterUser(filteredList);
  }, [keyword]);
    
  return (
    <div className="container">
      <div className="d-flex justify-content-between align-items-start mx-5 m-3">
        {}
        
        <h1>All User: {""}
          <span className="text-wraning">
            {filteredUser.length}
          </span>{""}
        </h1>
        <input className="form-control w-25" type="text" placeholder="Search..." onChange={(e)=>{setKeyword(e.target.value); }} />
      </div>
      <div>
          {
              (isLoading) ?
                  <div className="d-flex         justify-content-center">
                          <TailSpin
                            height="80"
                            width="80"
                            color="#4fa94d"
                            ariaLabel="tail-spin-loading"
                            radius="1"
                            wrapperStyle={{}}
                            wrapperClass=""
                            visible={true}
                          />
                    </div>
              :
                    <div className="row">
                      {filteredUser.length==0 ? (
                        <div>
                            <h3 className="text-center">No result</h3>
                        </div>
                      ) : (
                        <></>
                      )}

                      {filteredUser.map((user)=>(
                        <div className="col-4 d-flex justify-content-center"><UserCard user={user}/></div>
                      ))}
                 
                  </div>
            }
      </div>
      
    </div>
  )
}

export default AllofUser