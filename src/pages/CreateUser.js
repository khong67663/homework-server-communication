
import React from 'react';
import { useState } from 'react';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CREATE_USER } from '../data/userData';
import {ThreeDots} from "react-loader-spinner";
import { file_Upload } from '../data/fileData';


const CreateUser = () => {
  const[Isloading,setLoading]=useState(false);
  const[users,setUsers]=useState({});
  const[name,setName]=useState("");
  const[email,setEmail]=useState("");
  const[password,setPassword]=useState("");
  const[role,setRole]=useState("");
  const[avatar,setAvatar]=useState("");
  const[image,setImage]=useState("");
  const[selectedImage,setSelectImage]=useState("");

  let NewUsers ={
    "name":name,
    "email":email,
    "password":password,
    "role":role,
    "avatar":avatar,
  };

  const handleCreateUser =()=>{
    setLoading(true);
    // selected image
    if(selectedImage){
      // upload image
      // return response from upload image
      const data = new FormData();
      data.append("file",selectedImage);
      file_Upload(data).then((respose)=>
          {setAvatar(respose.location)

            NewUsers.avatar=respose.location
            // create new user
              CREATE_USER(NewUsers)
              .then ((respose)=>{
                setLoading(false);
                toast("Successfully created New User"); 
                handleClear();
              })
              .catch((error)=>{
                toast(error.respose.data.message[0]);
                setLoading(false);
              })

          })
      .catch((error)=>{console.log(error)});
    } else{
      NewUsers.avatar="https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png"
      CREATE_USER(NewUsers)
        .then ((respose)=>{console.log("testing:",respose); 
          setLoading(false);
          toast("Successfully created New User"); 
          handleClear();
        })
        .catch((error)=>{console.log("error:",error);
          toast(error.respose.data.message[0]);
          setLoading(false);
        })
    }
  }

  const handleClear =()=>{
    // toast("clear is click")
    setName("");
    setEmail("");
    setPassword("");
    setRole("");
    setAvatar("");
    setImage("");
  }

  const handelImageChange=(event)=>{
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (e) => {
        setImage(e.target.result);
      };
      reader.readAsDataURL(event.target.files[0]);
      setSelectImage(event.target.files[0]);
    }
  };

  

  // let buttonStatus = name&&email&&password&&role&&avatar ? false : true;
  let buttonStatus=false;
  console.log("butonStatus:",buttonStatus)

  console.log("New User:",NewUsers);

  return (
    <div className="container ">
      <div className="wrapper d-flex mt-5 bg-light rounded-3">
        
        <div className="image-side w-50 d-flex justif-content-center align-items-center flex-column">
            <img className="object-contain-fit" width="300px"  src={image? image : "https://sxprotection.com.au/wp-content/uploads/2016/07/team-placeholder.png"} alt="user" />
            <input type="file" onChange={handelImageChange} className="form-control w-50" />
        </div>

        <div className="input-side w-50">
          <h2>Create User</h2>
          <div className="form-group row mt-3 pe-5">  
            <div className="col">
              <label htmlFor="">UserName</label>
              <input className="form-control" type="text" placeholder="your name" name="" id="" value={name} onChange={(e)=>setName(e.target.value)}/> 
            </div>
            <div className='col'>
              <label htmlFor="">Email</label>
              <input className="form-control" type="text" placeholder="email" value={email} onChange={(e)=>setEmail(e.target.value)} />
            </div>
          </div>
            
          <div className="form-group row mt-3 pe-5">
            <div className="col">
              <label>Password</label>
              <input className='form-control' type="password" placeholder='password'value={password} onChange={(e)=>setPassword(e.target.value)}/>
            </div>
            <div className="col">
              <label htmlFor="">Roles</label>
              <input className="form-control" type="text" placeholder='roles' value={role} onChange={(e)=>setRole(e.target.value)}/>
            </div>
          </div>  

          {/* <div className="mt-3 pe-5">
            <label htmlFor="">Avatar Link:</label>
            <input className="form-control" type="text" placeholder="link" value={avatar} onChange={(e)=>setAvatar(e.target.value)}/>
          </div> */}

          <div className="d-flex justify-content-evenly">
            <button className="btn btn-danger mt-3" value={avatar} disabled={buttonStatus} onClick={handleCreateUser}>{" "}Create User{" "}</button>

            {Isloading ? (
                <ThreeDots 
                  height="80" 
                  width="80" 
                  radius="9"
                  color="#4fa94d" 
                  ariaLabel="three-dots-loading"
                  wrapperStyle={{}}
                  wrapperClassName=""
                  visible={true}
                 />
                 ) : (
                  <></>
            )}

            <button className="btn btn-warning mt-3" onClick={handleClear}>Clear</button>
          </div>
          <ToastContainer/>
        </div>
      </div>
    </div>
  )
}

export default CreateUser;