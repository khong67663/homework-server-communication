import { Toast } from 'bootstrap';
import React, { useEffect } from 'react';
import { useState } from 'react';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { CREATE_USER, GET_ONE } from '../data/userData';
import {ThreeDots} from "react-loader-spinner";
import { useParams } from 'react-router-dom';


const ViewUserProfile = () => {
  const[Isloading,setLoading]=useState(false);
  const[users,setUsers]=useState({});
  const[name,setName]=useState("");
  const[email,setEmail]=useState("");
  const[password,setPassword]=useState("");
  const[role,setRole]=useState("");
  const[avatar,setAvatar]=useState("");
  const{id}=useParams();

  let NewUsers ={
    "name":name,
    "email":email,
    "password":password,
    "role":role,
    "avatar":avatar,
  };

  useEffect(()=>{
    GET_ONE(id).then(response=>{
      setUsers(response)
      console.log("Get Response:",response)
      setName(response.name)
      setEmail(response.email)
      setPassword(response.password)
      setRole(response.role)
     

    })
    .catch (err=>console.log("error is:",err))
  },[])



  console.log("New User:",NewUsers);

  return (
    <div className="container ">
      <div className="wrapper d-flex mt-5 p-4 bg-light rounded-3 align-items-center">
        
        <div className="image-side w-50 d-flex justif-content-center align-items-center flex-column">
            <img className="object-contain-fit" width="300px"  src={users.avatar} alt="user" />
            <input type="file" className="form-control w-50" />
        </div>

        <div className="input-side w-50 ">
          <h2>User Information {id}</h2>
          <div className="form-group row mt-3 pe-5">  
            <div className="col">
              <label htmlFor="">UserName</label>
              <input className="form-control" type="text" placeholder="your name" name="" id="" value={name} onChange={(e)=>setName(e.target.value)}/> 
            </div>
            <div className='col'>
              <label htmlFor="">Email</label>
              <input className="form-control" type="text" placeholder="email" value={email} onChange={(e)=>setEmail(e.target.value)} />
            </div>
          </div>
            
          <div className="form-group row mt-3 pe-5">
            <div className="col">
              <label>Password</label>
              <input className='form-control' type="password" placeholder='password'value={password} onChange={(e)=>setPassword(e.target.value)}/>
            </div>
            <div className="col">
              <label htmlFor="">Roles</label>
              <input className="form-control" type="text" placeholder='roles' value={role} onChange={(e)=>setRole(e.target.value)}/>
            </div>
          </div>  

          <div className="d-flex justify-content-evenly">
            <button className="btn btn-danger mt-3" value={avatar} disabled >{" "}Update{" "}</button>

            <button className="btn btn-warning mt-3" >Cancel</button>
          </div>
          <ToastContainer/>
        </div>
      </div>
    </div>
  )
}

export default ViewUserProfile;